# IPL_Data_Project_1


1.  Clone the repository from the Gitlab:
```
git clone git@gitlab.com:BonyMondal98/ipl_data_project_1.git
```
2. The Data is downloaded from:

```
https://www.kaggle.com/manasgarg/ipl
```
3. The directory where the main functions are located:
```
src/server/ipl.js
```
4. The conversion of CSV to JSON:
```
src/server/index.js
```
5. CSVtoJSON node module ([installation](https://www.npmjs.com/package/csvtojson)):
```
npm i --save csvtojson
```
6. Node Modules:

* Once you have installed the **node module** all the dependencies of that module will store in **package-lock.json**
* The node_modules folder contains libraries downloaded from npm. which can be reused throughout the Node.js application.

## Problems:

In this data assignment you will transform raw data of IPL to calculate the following stats:
```

1. Number of matches played per year for all the years in IPL.

2. Number of matches won per team per year in IPL.

3. Extra runs conceded per team in the year 2016.

4. Top 10 economical bowlers in the year 2015.

Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder.
```
