module.exports = {
    matchesperyear: function (datamatch) {    //Problem 1
        const numofmatchesperyear = datamatch.reduce((numofmatchesperyear, match) => {
            numofmatchesperyear[match['season']] = (numofmatchesperyear[match['season']] || 0) + 1;
            return numofmatchesperyear;
        }, {}
        );
        return numofmatchesperyear;
    },
    matcheswonperteamperyear: function (datamatch) { // Problem 2
        let object = {};
        datamatch.forEach((element) => {
            if (object.hasOwnProperty(element.winner)) {
                object[element.winner].forEach((ele) => {
                    if (ele.hasOwnProperty(element.season)) {
                        ele[element.season] += 1;
                    }
                    else {
                        ele[element.season] = 1;
                    }

                });

            }
            else {
                object2 = {};
                object[element.winner] = [];
                object2[element.season] = 1;
                object[element.winner].push(object2);
            }

        });
        return object;
    },
    Extrarunsconcededperteamintheyear2016: function (datamatch, datadeliver) {  //Problem 3
        let object = {};
        datadeliver.forEach((element) => {
            if (element.extra_runs != 0) {
                datamatch.forEach((ele) => {
                    if (ele.id === element.match_id) {
                        if (ele.season === "2016") {
                            if (object.hasOwnProperty(element.bowling_team)) {
                                object[element.bowling_team] += parseInt(element.extra_runs);
                            }
                            else {
                                object[element.bowling_team] = parseInt(element.extra_runs);

                            }
                        }
                    }

                });

            }

        });
        return object;

    },
    Top10EconomicalBowlersInTheYear2015: function (datamatch, datadeliver) { //Problem 4
        const runsbyplayer = {};
        const ballsbyplayer = {};
        datadeliver.forEach((element) => {
            datamatch.forEach((ele) => {
                if (element.match_id === ele.id) {
                    if (ele.season === "2015") {
                        if (!runsbyplayer.hasOwnProperty(element.bowler)) {
                            runsbyplayer[element.bowler] = parseInt(element.total_runs);
                        }
                        else {
                            runsbyplayer[element.bowler] += parseInt(element.total_runs);
                        }
                        if (!ballsbyplayer.hasOwnProperty(element.bowler)) {
                            ballsbyplayer[element.bowler] = 1;
                        }
                        else {
                            ballsbyplayer[element.bowler] += 1;
                        }
                        if (Number(element.wide_runs) > 0 || Number(element.noball_runs) > 0) {

                            ballsbyplayer[element.bowler] -= 1;
                        }

                    }

                }

            });

        });
        let catchRes = [];
        for (let values in runsbyplayer) {
            //console.log(i)
            catchRes.push([values, Number(Number.parseFloat(runsbyplayer[values] / (ballsbyplayer[values] / 6)).toFixed(2))]);
        }
        catchRes.sort((a, b) => { return a[1] - b[1] }); //sorting for getting Top 10 players and economy

        let result = [];

        for (let index = 0; index < 10; index++) {

            result.push({ 'bowler': catchRes[index][0], 'Economy': catchRes[index][1] }); //storing into resultant array in the form of [{name: xyz , Economy:number}]
        }
        return result;
    }

}