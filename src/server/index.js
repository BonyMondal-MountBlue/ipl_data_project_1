// const matches = '../data/matches.csv';//Path to CSV file
// const deliver = '../data/deliveries.csv';//Path to CSV file
// const csv = require('csvtojson');
// csv()
//     .fromFile(matches)
//     .then((jsonObj) => {
//         //console.log(jsonObj);
//         const FileSystem = require("fs");
//         FileSystem.writeFile('../data/datamatches.json', JSON.stringify(jsonObj), (error) => {
//             if (error) throw error;
//         });

//     })
// csv()
//     .fromFile(deliver)
//     .then((jsonObj1) => {
//         //console.log(jsonObj1);
//           const FileSystem = require("fs");
//         FileSystem.writeFile('../data/datadeliveries.json', JSON.stringify(jsonObj1), (error) => {
//             if (error) throw error;
//         });
//     })
const Path = require('path');
const fs = require('fs');
//let datamatch=JSON.parse(fs.readFileSync('../data/datamatches.json', 'utf-8'));//Path to json file.
//console.log(datamatch)
//let datadeliver=JSON.parse(fs.readFileSync('../data/datadeliveries.json', 'utf-8'));//Path to json file.
//console.log((datadeliver));
const {
    matchesperyear,
    matcheswonperteamperyear,
    Extrarunsconcededperteamintheyear2016,
    Top10EconomicalBowlersInTheYear2015,
} = require('./ipl'); // here we are taking all the functions from ipl.js
const path = require('path');

// Matches Per Year | Problem: 1
// here we pass datamatch.json file as a parameter and result .json file dump into output file. 
// fs.writeFile('../public/output/matchesPerYear.json', JSON.stringify(result.matchesperyear(datamatch)), 'utf8', function (err) {
//     if (err) {
//         console.log("An error occured while writing JSON Object to File.");
//         return console.log(err);
//     }

//     console.log("JSON file has been saved: 1");
// });

// Matches Won Per Team Per Year | Problem: 2 
// here we pass only datamatch.json file as a parameter and result .json file dump into output.
// fs.writeFile('../public/output/MatchesWonPerTeamPerYear.json', JSON.stringify(result.matcheswonperteamperyear(datamatch)), 'utf8', function (err) {
//     if (err) {
//         console.log("An error occured while writing JSON Object to File.");
//         return console.log(err);
//     }

//     console.log("JSON file has been saved: 2");
// });

// Extra Run Conceded Per Team In 2016 | Problem: 3
// here we pass datamatch.json and datadeliveries.json file as a parameter and result dump into a output .json file 

// fs.writeFile('../public/output/ExtraRunConcededePerTeamIn2016.json', JSON.stringify(result.Extrarunsconcededperteamintheyear2016(datamatch,datadeliver)), 'utf8', function (err) {
//     if (err) {
//         console.log("An error occured while writing JSON Object to File.");
//         return console.log(err);
//     }

//     console.log("JSON file has been saved: 3");
// });

// Top 10 Economical Bowlers In The Year of 2015 | Problem: 4
// here we pass datamatch.json and datadeliveris.json files as a parameter and result .json file is dump into the output file.

// fs.writeFile('../public/output/Top10EconomicalBowlersInTheYear2015.json', JSON.stringify(result.Top10EconomicalBowlersInTheYear2015(datamatch, datadeliver)), 'utf8', function (err) {
//     if (err) {
//         console.log("An error occured while writing JSON Object to File.");
//         return console.log(err);
//     }

//     console.log("JSON file has been saved: 4");
// });
const DeliveriesData = require(path.join(__dirname, "../data/datadeliveries.json"));

const MatchesData = require(path.join(__dirname,'../data/datamatches.json'));

const ExecuteTheFunction = (
    MatchesData,
    DeliveriesData,
    OutputFilename,
    cb
) => {
    const IPLresult = cb(MatchesData, DeliveriesData);
    const OutputFilePath = path.join(__dirname, `../public/output/${OutputFilename}.json`);
    fs.writeFile(OutputFilePath, JSON.stringify(IPLresult), (err) => {
        if (err){
            throw err;
        }
        else{
            console.log("JSON file has been Saved");
        }
    })
};
ExecuteTheFunction(MatchesData,DeliveriesData,"matchesPerYear",matchesperyear);// using .reduce method
ExecuteTheFunction(MatchesData,DeliveriesData,"MatchesWonPerTeamPerYear",matcheswonperteamperyear);
ExecuteTheFunction(MatchesData,DeliveriesData,"Top10EconomicalBowlersInTheYear2015",Top10EconomicalBowlersInTheYear2015);
ExecuteTheFunction(MatchesData,DeliveriesData,"ExtraRunConcededePerTeamIn2016",Extrarunsconcededperteamintheyear2016);