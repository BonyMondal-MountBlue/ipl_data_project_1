let CSVtoJSON=require('convert-csv-to-json');

let DeliveriesCSVfile='../src/data/deliveries.csv';

let MatchesCSVfile='../src/data/matches.csv';

let DeliveriesJSONfile='../src/data/datadeliveries.json';

let MatchesJSONfile='../src/data/datamatches.json'

CSVtoJSON.fieldDelimiter(',').generateJsonFileFromCsv(DeliveriesCSVfile,DeliveriesJSONfile);
CSVtoJSON.fieldDelimiter(',').generateJsonFileFromCsv(MatchesCSVfile,MatchesJSONfile);
